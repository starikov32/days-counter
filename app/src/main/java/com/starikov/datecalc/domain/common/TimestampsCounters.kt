package com.starikov.datecalc.domain.common

data class TimestampCounters(
    var years: Int? = null,
    var months: Int? = null,
    var weeks: Int? = null,
    var days: Int? = null
) {
    fun isSelected(method: CountingMethod) =
        when (method) {
            CountingMethod.YEARS -> years
            CountingMethod.MONTHS -> months
            CountingMethod.WEEKS -> weeks
            CountingMethod.DAYS -> days
        } != null
}