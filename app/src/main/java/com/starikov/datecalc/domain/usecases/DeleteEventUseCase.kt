package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.domain.common.Event
import javax.inject.Inject

class DeleteEventUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    suspend fun execute(event: Event) {
        eventsRepository.deleteEvent(event)
    }
}