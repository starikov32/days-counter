package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetEventByWidgetIdUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    fun execute(id: Int) = eventsRepository.getEventByWidgetId(id)
}