package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MakeEventsBackupUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    suspend fun execute(): String? {
        return eventsRepository.makeEventsBackup()
    }
}