package com.starikov.datecalc.domain.common

enum class DateFormatStyle {
    DEFAULT,
    FULL,
    SHORT_FIRST_DAY_SLASH,
    SHORT_FIRST_YEAR_SLASH,
    SHORT_FIRST_DAY_DOT,
    SHORT_FIRST_YEAR_DOT;

    companion object {
        fun fromDateFormatStyle(dateFormatStyle: DateFormatStyle) =
            when(dateFormatStyle) {
                DEFAULT -> "1"
                FULL -> "2"
                SHORT_FIRST_DAY_SLASH -> "3"
                SHORT_FIRST_YEAR_SLASH -> "4"
                SHORT_FIRST_DAY_DOT -> "5"
                SHORT_FIRST_YEAR_DOT -> "6"
            }

        fun toDateFormatStyle(string: String) =
            when(string) {
                "1" -> DEFAULT
                "2" -> FULL
                "3" -> SHORT_FIRST_DAY_SLASH
                "4" -> SHORT_FIRST_YEAR_SLASH
                "5" -> SHORT_FIRST_DAY_DOT
                "6" -> SHORT_FIRST_YEAR_DOT
                else -> DEFAULT
            }
    }
}