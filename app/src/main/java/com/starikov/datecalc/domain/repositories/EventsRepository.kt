package com.starikov.datecalc.domain.repositories

import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.DisplaySettings
import com.starikov.datecalc.domain.common.InsertionMethod
import kotlinx.coroutines.flow.Flow

interface EventsRepository {
    fun getEventsList(): Flow<List<Event>>
    fun getEvent(id: Long): Flow<Event>
    fun hasEvents(): Flow<Boolean>
    fun getEventByWidgetId(widgetId: Int): Flow<Event>

    suspend fun addEvent(event: Event)
    suspend fun addEventsConflictIgnore(events: List<Event>)
    suspend fun addEventsConflictReplace(events: List<Event>)

    suspend fun editEvent(event: Event)
    suspend fun insertEventDisplaySettings(displaySettings: DisplaySettings)
    suspend fun editEventsDisplaySettings(displaySettingsList: List<DisplaySettings>)

    suspend fun deleteEvent(event: Event)
    suspend fun deleteAllEvents()

    suspend fun makeEventsBackup() : String?
    suspend fun applyEventsBackup(uri: String, method: InsertionMethod?) : Int?
}