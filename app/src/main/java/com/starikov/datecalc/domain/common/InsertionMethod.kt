package com.starikov.datecalc.domain.common

enum class InsertionMethod {
    REPLACE,
    IGNORE,
    SCRATCH
}