package com.starikov.datecalc.domain.common

import android.graphics.Color

enum class DefinedColors(val int: Int) {
    ORANGE(Color.parseColor("#f37f3d")),
    RED(Color.parseColor("#E53935")),
    PURPLE(Color.parseColor("#673AB7")),
    BLUE(Color.parseColor("#1E88E5")),
    GREEN(Color.parseColor("#43A047")),
    YELLOW(Color.parseColor("#FDD835")),
    CUSTOM(Color.parseColor("#7CB342"));

    companion object {
        val DEFAULT_COLOR = ORANGE

        fun convert(color: Int) = when (color) {
            Color.parseColor("#f37f3d") -> ORANGE
            Color.parseColor("#E53935") -> RED
            Color.parseColor("#673AB7") -> PURPLE
            Color.parseColor("#1E88E5") -> BLUE
            Color.parseColor("#43A047") -> GREEN
            Color.parseColor("#FDD835") -> YELLOW
            else -> CUSTOM
        }
    }
}