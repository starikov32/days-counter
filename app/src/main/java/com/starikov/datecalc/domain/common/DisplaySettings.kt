package com.starikov.datecalc.domain.common

import java.util.*

data class DisplaySettings(
    val eventId: Long,
    val countingMethods: Set<CountingMethod>,
    var orderId: Long,
    val color: Int
) {
    companion object {
        fun default(eventId: Long): DisplaySettings {
            return DisplaySettings(
                    eventId = eventId,
                    countingMethods = CountingMethod.DEFAULT_COUNTING_METHODS,
                    orderId = Date().time,
                    color = DefinedColors.DEFAULT_COLOR.int
            )
        }
    }
}