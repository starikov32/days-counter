package com.starikov.datecalc.domain.common

enum class LoadStatus {
    FILE_NOT_SELECTED,
    SUCCESS,
    FAIL,
    IN_PROCESS,
    NOT_STARTED
}