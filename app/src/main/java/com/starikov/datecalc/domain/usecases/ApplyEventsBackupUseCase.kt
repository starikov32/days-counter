package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.domain.common.InsertionMethod
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApplyEventsBackupUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    suspend fun execute(uri: String, method: InsertionMethod?): Int? {
        return eventsRepository.applyEventsBackup(uri, method)
    }
}