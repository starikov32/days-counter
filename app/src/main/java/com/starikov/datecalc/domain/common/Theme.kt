package com.starikov.datecalc.domain.common

enum class Theme {
    DARK_THEME,
    DEFAULT_THEME,
    SYSTEM_THEME
}