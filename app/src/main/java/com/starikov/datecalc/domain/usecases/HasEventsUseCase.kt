package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HasEventsUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    fun execute() = eventsRepository.hasEvents()
}