package com.starikov.datecalc.domain.common

import java.util.*

data class Event(
    val id: Long,
    var title: String,
    var description: String,
    val date: Date,
    val additionOrder: Long,
    val widgetId: Int,
    val displaySettings: DisplaySettings
) {
    companion object {
        fun default(): Event {
            return Event(
                id = 0,
                title = "",
                description = "",
                date = Calendar.getInstance().time,
                additionOrder = System.currentTimeMillis(),
                widgetId = 0,
                displaySettings = DisplaySettings.default(0)
            )
        }
    }
}