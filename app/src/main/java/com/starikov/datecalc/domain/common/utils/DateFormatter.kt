package com.starikov.datecalc.domain.common.utils

import com.starikov.datecalc.domain.common.DateFormatStyle
import com.starikov.datecalc.domain.common.DateFormatStyle.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateFormatter(
    private val dateFormatStyle: DateFormatStyle
) {
    fun format(date: Date): String =
        when(dateFormatStyle) {
            DEFAULT -> DateFormat.getDateInstance().format(date)
            FULL -> DateFormat.getDateInstance(DateFormat.LONG).format(date)
            SHORT_FIRST_DAY_SLASH -> SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(date)
            SHORT_FIRST_YEAR_SLASH -> SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(date)
            SHORT_FIRST_DAY_DOT -> SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(date)
            SHORT_FIRST_YEAR_DOT -> SimpleDateFormat("yyyy.MM.dd", Locale.getDefault()).format(date)
        }
}