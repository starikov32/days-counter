package com.starikov.datecalc.domain.common.utils

import android.appwidget.AppWidgetManager
import com.starikov.datecalc.R
import com.starikov.datecalc.domain.common.CountingMethod
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.TimestampCounters
import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import kotlin.math.floor

fun Event.countDaysToToday() =
    ChronoUnit.DAYS.between(LocalDate.now(), getLocalDate()).toInt()

fun Event.getEventTimeStatusResId(): Int {
    return if (countDaysToToday() >= 0) {
        R.plurals.time_left
    } else {
        R.plurals.passed_time
    }
}

private fun Event.getLocalDate() = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()

fun Event.getStartDate(): LocalDate {
    val localDate = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()
    return if (localDate > LocalDate.now()) {
        LocalDate.now()
    } else {
        localDate
    }
}

fun Event.getFinishDate(): LocalDate {
    val localDate = date.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDate()
    return if (localDate < LocalDate.now()) {
        LocalDate.now()
    } else {
        localDate
    }
}

fun Event.hasWidget() =
    widgetId != AppWidgetManager.INVALID_APPWIDGET_ID

fun Event.countForEvent(
        countingMethods: Set<CountingMethod>
): TimestampCounters {
    val result = TimestampCounters()

    var newStartDate = getStartDate()
    val finishDate = getFinishDate()
    val period = Period.between(newStartDate, finishDate)

    if (countingMethods.contains(CountingMethod.YEARS)) {
        result.years = period.years
        newStartDate = newStartDate.plusYears(result.years!!.toLong())
    }

    if (countingMethods.contains(CountingMethod.MONTHS)) {
        result.months = period.months + if (result.years == null) {
            period.years * 12
        } else 0
        newStartDate = newStartDate.plusMonths(result.months!!.toLong())
    }

    var newDays = ChronoUnit.DAYS.between(newStartDate, finishDate).toInt()

    if (countingMethods.contains(CountingMethod.WEEKS)) {
        result.weeks = floor(newDays / 7.toDouble()).toInt()
        newDays -= result.weeks!! * 7
    }

    if (countingMethods.contains(CountingMethod.DAYS)) {
        result.days = newDays
    }

    return result
}