package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.domain.common.DisplaySettings
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EditEventsDisplaySettingsUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    suspend fun execute(displaySettingsList: List<DisplaySettings>) {
        eventsRepository.editEventsDisplaySettings(displaySettingsList)
    }
}