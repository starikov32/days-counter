package com.starikov.datecalc.domain.usecases

import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.domain.common.Event
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddEventUseCase @Inject constructor(
        private val eventsRepository: EventsRepository
) {
    suspend fun execute(event: Event) {
        eventsRepository.addEvent(event)
    }
}