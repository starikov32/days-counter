package com.starikov.datecalc.domain.common

enum class CountingMethod(val mask: Int) {
    YEARS(0b1000),
    MONTHS(0b0100),
    WEEKS(0b0010),
    DAYS(0b0001);

    companion object {
        val DEFAULT_COUNTING_METHODS = setOf(DAYS)
    }
}