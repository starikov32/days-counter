package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.*
import com.starikov.datecalc.domain.usecases.EditEventUseCase
import com.starikov.datecalc.domain.usecases.GetEventUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class EditEventViewModel @Inject constructor(
        private val editEventUseCase: EditEventUseCase,
        getEventUseCase: GetEventUseCase,
        savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val eventId: Long = savedStateHandle.get<Long>("eventId")!!

    val event = getEventUseCase.execute(eventId).asLiveData() as MutableLiveData

    fun isRequiredFieldsFilled() = !event.value?.title.isNullOrEmpty()

    fun changeEventDate(year: Int, month: Int, dayOfMonth: Int) {
        val newDate = Calendar.getInstance().apply { set(year, month, dayOfMonth) }.time
        event.value?.copy(date = newDate)?.let { new ->
            event.value = new
        }
    }

    fun saveEditedEvent() {
        viewModelScope.launch {
            event.value?.let { editEventUseCase.execute(it) }
        }
    }
}