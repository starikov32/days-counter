package com.starikov.datecalc.presentation.fragments

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.starikov.datecalc.BuildConfig
import com.starikov.datecalc.R
import com.starikov.datecalc.databinding.FragmentUploadToFileBinding
import com.starikov.datecalc.presentation.viewmodels.UploadToFileViewModel
import dagger.hilt.android.AndroidEntryPoint

import java.io.File

@AndroidEntryPoint
class UploadToFileFragment : Fragment() {

    private lateinit var binding: FragmentUploadToFileBinding
    private val viewModel: UploadToFileViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentUploadToFileBinding.inflate(inflater, container, false).apply {
            viewModel = this@UploadToFileFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            buttonStart.setOnClickListener {
                if (necessaryPermissionsGranted()) {
                    actionStart()
                } else {
                    requestNecessaryPermissions()
                }
            }
            fabSend.setOnClickListener { actionSend() }
            subscribeUI()
        }

        return binding.root
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        val isGranted = (
            requestCode == PERMISSIONS_CODE &&
                grantResults.all { it == PackageManager.PERMISSION_GRANTED } )
        if (isGranted) {
            actionStart()
        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.load_file_permissions_denied),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun subscribeUI() {
        viewModel.uploadStatus.observe(viewLifecycleOwner) { }
    }

    private fun actionStart() {
        viewModel.uploadEventsToFile()
    }

    private fun actionSend() {
        viewModel.backupFilename?.let { fileName ->
            if (fileName.isNotEmpty()) {
                val uri = FileProvider.getUriForFile(
                        requireContext(), BuildConfig.APPLICATION_ID, File(fileName))
                val intent = getSendingFileIntent(uri)
                startActivity(Intent.createChooser(intent, getString(R.string.action_with_backup_file)))
            }
        }
    }

    private fun getSendingFileIntent(uri: Uri) =
        Intent().apply {
            action = Intent.ACTION_SEND
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
            setDataAndType(uri, "text/*")
            putExtra(Intent.EXTRA_STREAM, uri)
        }

    private fun necessaryPermissionsGranted() =
        NECESSARY_PERMISSIONS.all {
            ActivityCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
        }

    private fun requestNecessaryPermissions() {
        requestPermissions(NECESSARY_PERMISSIONS, PERMISSIONS_CODE)
    }

    companion object {
        private val NECESSARY_PERMISSIONS = arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        private const val PERMISSIONS_CODE = 1
    }
}
