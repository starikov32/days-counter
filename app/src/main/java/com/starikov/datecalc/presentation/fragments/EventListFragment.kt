package com.starikov.datecalc.presentation.fragments

import android.app.AlertDialog
import android.appwidget.AppWidgetManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

import com.starikov.datecalc.R
import com.starikov.datecalc.presentation.adapters.EventListAdapter
import com.starikov.datecalc.presentation.adapters.SimpleItemTouchHelperCallback
import com.starikov.datecalc.data.AppPreferences
import com.starikov.datecalc.data.LocalPreferences
import com.starikov.datecalc.databinding.FragmentEventListBinding
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.utils.hasWidget
import com.starikov.datecalc.domain.common.utils.DateFormatter
import com.starikov.datecalc.presentation.utils.isVisible
import com.starikov.datecalc.presentation.viewmodels.EventListViewModel
import com.starikov.datecalc.presentation.widgets.EventWidget
import com.starikov.datecalc.presentation.viewmodels.EventListViewModel.Companion.SortType
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class EventListFragment : Fragment() {

    private val viewModel: EventListViewModel by viewModels()

    private val navController by lazy { findNavController()}
    @Inject
    lateinit var localPreferences: LocalPreferences
    @Inject
    lateinit var appPreferences: AppPreferences
    private lateinit var binding: FragmentEventListBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        binding = FragmentEventListBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            recyclerView.adapter = eventListAdapter
            val itemTouchHelper =
                ItemTouchHelper(SimpleItemTouchHelperCallback(viewModel))
            itemTouchHelper.attachToRecyclerView(recyclerView)
            // листенер для скрытия fab если список прокручен вниз
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (localPreferences.useRoundIcon) {
                        if (dy > 0) fabAdd.hide() else fabAdd.show()
                    }
                    super.onScrolled(recyclerView, dx, dy)
                }
            })
            fabAdd.setOnClickListener { actionAddEvent() }
            subscribeUi(eventListAdapter)
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        val dateFormatStyle = appPreferences.dateFormatStyle
        binding.todayTextView.text = getString(R.string.today, DateFormatter(dateFormatStyle).format(Date()))
        binding.fabAdd.isVisible(localPreferences.useRoundIcon)
    }

    override fun onPause() {
        super.onPause()
        viewModel.saveSort()
    }

    private fun actionAddEvent() = navController.navigate(R.id.action_add_event)

    private val eventClickListener = View.OnClickListener {
        // получаем событие, оно был установлен ранее в onBindViewHolder адаптера
        val event = it.tag as Event
        val direction = EventListFragmentDirections.actionEventDetails(event.id)
        navController.navigate(direction)
    }

    private val eventListAdapter = EventListAdapter().apply {
        onItemClickListener = eventClickListener
    }

    private fun deleteWidgetForEvent(event: Event) {
        if (event.hasWidget()) {
            EventWidget.updateWidget(
                requireContext().applicationContext,
                AppWidgetManager.INVALID_APPWIDGET_ID,
                event
            )
        }
    }

    private fun subscribeUi(adapter: EventListAdapter) {
        viewModel.events.observe(viewLifecycleOwner) { list ->
            binding.hasEvents = !list.isNullOrEmpty()
            localPreferences.userHasEvents = !list.isNullOrEmpty()
            list?.let { adapter.submitList(it) }
        }

        viewModel.deletedEvent.observe(viewLifecycleOwner) {
            if (it != null) {
                showRestoreSnackbar(it)
            } else {
                eventListAdapter.submitList(viewModel.events.value)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_event_list_fragment, menu)
        menu.findItem(R.id.action_add_event).isVisible = !localPreferences.useRoundIcon
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sort -> {
            showSortDialog()
            true
        }
        R.id.action_add_event -> {
            actionAddEvent()
            true
        }
        else -> {
            item.onNavDestinationSelected(navController)
        }
    }

    private fun showRestoreSnackbar(event: Event) {
        // Дать возможность восстановить удаленное событие
        Snackbar.make(binding.root, R.string.event_deleted_warning, Snackbar.LENGTH_SHORT)
            // Удалять виджет, только после того, как пропадет возможность восстановить
            .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, id: Int) {
                    super.onDismissed(transientBottomBar, id)
                    // если не была нажата кнопка "Восстановить"
                    if (id != DISMISS_EVENT_ACTION){
                        deleteWidgetForEvent(event)
                    }
                    viewModel.deletedEvent.postValue(null)
                }
            })
            .setAction(R.string.event_restore) {
                viewModel.restoreLastDeletedEvent()
            }
            .show()
    }

    private fun showSortDialog() {
        AlertDialog.Builder(requireContext()).apply {
            setTitle(getString(R.string.action_sort))
            setItems(R.array.sort_options) { _, which ->
                val sortType = SortType.fromInt(which)
                sortType?.let { viewModel.sortBy(sortType) }
            }
            setNegativeButton(R.string.cancel, null)
            create()
        }.show()
    }
}
