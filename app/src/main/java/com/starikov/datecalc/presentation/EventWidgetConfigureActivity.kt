package com.starikov.datecalc.presentation

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil

import com.starikov.datecalc.R
import com.starikov.datecalc.data.LocalPreferences
import com.starikov.datecalc.presentation.adapters.EventListAdapter
import com.starikov.datecalc.databinding.ActivityEventWidgetConfigureBinding
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.presentation.viewmodels.ConfigureViewModel
import com.starikov.datecalc.presentation.widgets.EventWidget
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EventWidgetConfigureActivity : AppCompatActivity() {
    private val viewModel: ConfigureViewModel by viewModels()

    private var widgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private lateinit var resultValue: Intent
    lateinit var binding: ActivityEventWidgetConfigureBinding

    @Inject
    lateinit var localPreferences: LocalPreferences

    private var eventClickListener = View.OnClickListener { view ->
        val event = view.tag as Event
        viewModel.saveEditedEvent(event.copy(widgetId = widgetId))

        EventWidget.updateWidget(this, widgetId, event)

        this.setResult(Activity.RESULT_OK, resultValue)
        this.finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_event_widget_configure)
        binding.run {
            val adapter = EventListAdapter().apply { onItemClickListener = eventClickListener }
            recyclerView.adapter = adapter
            subscribeUi(adapter)
        }

        widgetId = intent.extras?.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID
        ) ?: AppWidgetManager.INVALID_APPWIDGET_ID
        if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
        }

        resultValue = Intent()
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId)

        // Чтобы при закрытии активити, система поняла что виджет создавать не нужно
        setResult(RESULT_CANCELED, resultValue)
    }

    private fun subscribeUi(adapter: EventListAdapter) {
        viewModel.events.observe(this) { list ->
            binding.hasEvents = !list.isNullOrEmpty()
            list?.let { adapter.submitList(it) }
        }
    }
}
