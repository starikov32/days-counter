package com.starikov.datecalc.presentation.utils

import android.view.View

fun View.isVisible(visible: Boolean) {
    if (visible) visibility = View.VISIBLE else View.GONE
}