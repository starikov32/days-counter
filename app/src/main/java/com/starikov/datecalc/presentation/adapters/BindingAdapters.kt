@file:Suppress("DEPRECATION")

package com.starikov.datecalc.presentation.adapters

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.databinding.BindingAdapter
import com.starikov.datecalc.R
import com.starikov.datecalc.data.AppPreferences
import com.starikov.datecalc.domain.common.DefinedColors
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.LoadStatus
import com.starikov.datecalc.domain.common.TimestampCounters
import com.starikov.datecalc.domain.common.utils.DateFormatter
import com.starikov.datecalc.domain.common.utils.*
import com.starikov.datecalc.presentation.utils.isVisible
import java.util.*
import kotlin.math.absoluteValue

// Удобнее было использовать isGone чтобы упростить условия в xml
@BindingAdapter("isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    view.isVisible(!isGone)
}

@BindingAdapter("timestampsCounters")
fun bindTimestampsCounters(textView: TextView, timestampCounters: TimestampCounters?) {
    var string = ""
    timestampCounters?.let { tc ->
        tc.years?.let { string += "$it/" }
        tc.months?.let { string += "$it/" }
        tc.weeks?.let { string += "$it/" }
        tc.days?.let { string += "$it/" }
    }
    textView.text = if (string.isEmpty()) {
        "-"
    } else {
        string.dropLast(1)
    }
}

@BindingAdapter("eventTimestampsActivated")
fun bindTimestampsActivated(textView: TextView, timestampCounters: TimestampCounters?) {
    var string = ""
    val res = textView.resources
    timestampCounters?.let { tc ->
        tc.years?.let { string += "${res.getQuantityString(R.plurals.short_years, it)}/" }
        tc.months?.let { string += "${res.getQuantityString(R.plurals.short_months, it)}/" }
        tc.weeks?.let { string += "${res.getQuantityString(R.plurals.short_weeks, it)}/" }
        tc.days?.let { string += "${res.getQuantityString(R.plurals.short_days, it)}/" }
    }
    textView.text = if (string.isEmpty()) {
        "-"
    } else {
        string.dropLast(1)
    }
}

@BindingAdapter("eventTimeStatus")
fun bindEventTimeStatus(textView: TextView, event: Event?) {
    val string = event?.let {
        textView.resources.getQuantityString(it.getEventTimeStatusResId(), it.countDaysToToday().absoluteValue)
    }
    textView.text = string ?: ""
}

@BindingAdapter("android:text")
fun bindTextDate(textView: TextView, date: Date?) {
    date?.let {
        val dateFormatStyle = AppPreferences(textView.context).dateFormatStyle
        textView.text = DateFormatter(dateFormatStyle).format(it)
    }
}

@BindingAdapter("android:textColor")
fun bindLoadStatusTextColor(textView: TextView, status: LoadStatus?) {
    val color = when(status) {
        LoadStatus.SUCCESS -> textView.resources.getColor(R.color.colorSuccess)
        LoadStatus.FAIL -> textView.resources.getColor(R.color.colorFail)
        else -> textView.resources.getColor(R.color.colorAccent)
    }
    textView.setTextColor(color)
}

@BindingAdapter("android:progress")
fun bindLoadStatusProgress(progressBar: ProgressBar, status: LoadStatus?) {
    progressBar.progress = when(status) {
        LoadStatus.FAIL,
        LoadStatus.SUCCESS -> 2
        LoadStatus.IN_PROCESS -> 1
        else -> 0
    }
}

@BindingAdapter("android:text")
fun bindLoadStatusText(textView: TextView, status: LoadStatus?) {
    textView.text = when(status) {
        LoadStatus.SUCCESS -> textView.resources.getString(R.string.status_success)
        LoadStatus.FAIL -> textView.resources.getString(R.string.status_fail)
        LoadStatus.IN_PROCESS -> textView.resources.getString(R.string.status_in_process)
        else -> ""
    }
}

@BindingAdapter("cardBackgroundColor")
fun bindDefinedColorsCardView(cardView: CardView, color: DefinedColors) {
    cardView.setCardBackgroundColor(color.int)
}