package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.*
import com.starikov.datecalc.domain.common.DisplaySettings
import com.starikov.datecalc.presentation.adapters.ItemTouchHelperAdapter
import com.starikov.datecalc.domain.usecases.*
import com.starikov.datecalc.domain.common.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EventListViewModel @Inject constructor(
        private val deleteEventUseCase: DeleteEventUseCase,
        private val addEventUseCase: AddEventUseCase,
        private val editEventsDisplaySettingsUseCase: EditEventsDisplaySettingsUseCase,
        getEventsUseCase: GetEventsUseCase,
) : ViewModel(), ItemTouchHelperAdapter {

    val deletedEvent = MutableLiveData<Event?>()

    val events = getEventsUseCase.execute().asLiveData() as MutableLiveData

    fun saveSort() =
        events.value?.map { it.displaySettings }?.let {
            viewModelScope.launch {
                editEventsDisplaySettingsUseCase.execute(it)
            }
        }

    fun sortBy(sortType: SortType) {
        val sortedList = sortList(sortType)
        val newDSList = events.value!!.map { it.displaySettings }.toMutableList()
        sortedList.forEachIndexed { index, event ->
            val ds = newDSList.find { it.eventId == event.id }
            if (ds == null) {
                newDSList.add(DisplaySettings.default(event.id).copy(orderId = index+1L))
            } else {
                ds.orderId = index+1L
            }
        }
        viewModelScope.launch { editEventsDisplaySettingsUseCase.execute(newDSList) }
    }

    private fun sortList(sortType: SortType) = when(sortType) {
        SortType.BY_TITLE_AZ -> events.value!!.sortedBy { it.title }
        SortType.BY_TITLE_ZA -> events.value!!.sortedByDescending { it.title }
        SortType.BY_DATE_ASC -> events.value!!.sortedBy { it.date }
        SortType.BY_DATE_DESC -> events.value!!.sortedByDescending { it.date }
        SortType.BY_ADD_DATE_ASC -> events.value!!.sortedBy { it.additionOrder }
        SortType.BY_ADD_DATE_DESC -> events.value!!.sortedByDescending { it.additionOrder }
    }

    override fun onItemMove(from: Int, to: Int): Boolean {
        if (from < to) {
            for (i in from until to) {
                swapEvents(i, i+1)
            }
        } else {
            for (i in from downTo to + 1) {
                swapEvents(i, i-1)
            }
        }
        return true
    }

    private fun swapEvents(i: Int, i1: Int) {
        val orderFrom = events.value!![i].displaySettings.orderId
        val orderTo = events.value!![i1].displaySettings.orderId
        events.value!![i].displaySettings.orderId = orderTo
        events.value!![i1].displaySettings.orderId = orderFrom
        events.value = events.value!!.sortedBy { it.displaySettings.orderId  }
    }

    override fun onItemDismiss(event: Event) {
        deletedEvent.postValue(event)
        viewModelScope.launch {
            deleteEventUseCase.execute(event)
        }
    }

    fun restoreLastDeletedEvent() {
        deletedEvent.value?.let { deleted ->
            viewModelScope.launch {
                addEventUseCase.execute(deleted)
                deletedEvent.postValue(null)
            }
        }
    }

    companion object {

        enum class SortType(val value: Int) {
            BY_TITLE_AZ(0),
            BY_TITLE_ZA(1),
            BY_DATE_ASC(2),
            BY_DATE_DESC(3),
            BY_ADD_DATE_ASC(4),
            BY_ADD_DATE_DESC(5);

            companion object {
                private val map = values().associateBy(SortType::value)
                fun fromInt(type: Int) = map[type]
            }
        }
    }
}