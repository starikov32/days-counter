package com.starikov.datecalc.presentation.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.starikov.datecalc.R
import com.starikov.datecalc.data.LocalPreferences
import com.starikov.datecalc.databinding.FragmentEditEventBinding
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.utils.hasWidget
import com.starikov.datecalc.presentation.utils.isVisible
import com.starikov.datecalc.presentation.viewmodels.EditEventViewModel
import com.starikov.datecalc.presentation.widgets.EventWidget
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class EditEventFragment : Fragment() {

    private val args: EditEventFragmentArgs by navArgs()
    private lateinit var binding: FragmentEditEventBinding
    private val navController by lazy { findNavController()}
    @Inject
    lateinit var localPreferences: LocalPreferences
    private val viewModel: EditEventViewModel by viewModels()

    // для того чтобы не открывалось больше одного диалога выбора даты
    private var datePickerOnScreen: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentEditEventBinding.inflate(inflater, container, false).apply {
            viewModel = this@EditEventFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            fabSave.setOnClickListener { actionSave() }
            setupDateEditText(this)
        }
        viewModel.event.observe(viewLifecycleOwner) {}

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        binding.fabSave.isVisible(localPreferences.useRoundIcon)
    }

    override fun onPause() {
        super.onPause()
        if (viewModel.isRequiredFieldsFilled()) {
            viewModel.saveEditedEvent()
            updateWidgetForEvent(viewModel.event.value!!)
        }
    }

    private fun setupDateEditText(binding: FragmentEditEventBinding) {
        binding.date.run {
            onFocusChangeListener = dateFocusChangeListener
            setOnClickListener(dateOnClickListener)
        }
        binding.pickDateButton.setOnClickListener(dateOnClickListener)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_edit_event_fragment, menu)
        menu.findItem(R.id.action_save).isVisible = !localPreferences.useRoundIcon
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> {
                actionSave()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun createDatePickerDialog() {
        datePickerOnScreen = true
        val date = viewModel.event.value?.date ?: Date()
        val calendar = Calendar.getInstance().apply { time = date }
        DatePickerDialog(
            requireContext(),
            datePickerSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).apply {
            setOnDismissListener { datePickerOnScreen = false }
        }.show()
    }

    private val datePickerSetListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        viewModel.changeEventDate(year, month, dayOfMonth)
    }

    private val dateFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
        if (hasFocus && !datePickerOnScreen) {
            createDatePickerDialog()
        }
    }

    private val dateOnClickListener = View.OnClickListener {
        if (!datePickerOnScreen) {
            createDatePickerDialog()
        }
    }

    private fun actionSave() {
        if (viewModel.isRequiredFieldsFilled()) {
            viewModel.saveEditedEvent()
            updateWidgetForEvent(viewModel.event.value!!)
            navController.popBackStack()
        } else {
            warningEmptyFields()
        }
    }

    private fun updateWidgetForEvent(event: Event) {
        if (event.hasWidget()) {
            EventWidget.updateWidget(
                requireContext().applicationContext,
                event.widgetId,
                event
            )
        }
    }

    private fun warningEmptyFields() {
        binding.title.requestFocus()
        Toast.makeText(requireContext(), R.string.warning_empty_fields, Toast.LENGTH_LONG).show()
    }
}

