package com.starikov.datecalc.presentation.adapters

interface ItemTouchHelperViewHolder {
    fun onItemSelected()
    fun onItemClear()
}