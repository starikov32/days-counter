package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.starikov.datecalc.domain.usecases.HasEventsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    hasEventsUseCase: HasEventsUseCase
) : ViewModel() {

    val hasEvents = hasEventsUseCase.execute().asLiveData()
}