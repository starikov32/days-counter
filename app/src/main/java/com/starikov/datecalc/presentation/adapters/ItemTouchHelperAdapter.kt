package com.starikov.datecalc.presentation.adapters

import com.starikov.datecalc.domain.common.Event

interface ItemTouchHelperAdapter {
    fun onItemMove(from: Int, to: Int): Boolean
    fun onItemDismiss(event: Event)
}