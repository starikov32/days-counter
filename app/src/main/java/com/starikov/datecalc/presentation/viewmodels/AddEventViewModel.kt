package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.*
import com.starikov.datecalc.domain.common.DisplaySettings
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.usecases.AddEventUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class AddEventViewModel @Inject constructor(
    private val addEventUseCase: AddEventUseCase
) : ViewModel() {

    val event: MutableLiveData<Event> = MutableLiveData(Event.default())

    fun isRequiredFieldsFilled() = !event.value?.title.isNullOrEmpty()

    fun changeEventDate(year: Int, month: Int, dayOfMonth: Int) {
        val newDate = Calendar.getInstance().apply { set(year, month, dayOfMonth) }.time
        event.value = (event.value?.copy(date = newDate))
    }

    fun addEvent() {
        viewModelScope.launch {
            event.value?.let {
                addEventUseCase.execute(it.copy(displaySettings = DisplaySettings.default(it.id)))
            }
        }
    }
}