package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.usecases.EditEventUseCase
import com.starikov.datecalc.domain.usecases.GetEventsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConfigureViewModel @Inject constructor(
        private val editEventUseCase: EditEventUseCase,
        getEventsUseCase: GetEventsUseCase,
) : ViewModel() {

    val events = getEventsUseCase.execute().asLiveData()

    fun saveEditedEvent(event: Event) {
        viewModelScope.launch {
            editEventUseCase.execute(event)
        }
    }
}