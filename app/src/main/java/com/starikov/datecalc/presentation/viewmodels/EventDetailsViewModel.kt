package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.*
import com.starikov.datecalc.domain.common.CountingMethod
import com.starikov.datecalc.domain.common.TimestampCounters
import com.starikov.datecalc.domain.common.DefinedColors
import com.starikov.datecalc.domain.common.utils.countForEvent
import com.starikov.datecalc.domain.usecases.DeleteEventUseCase
import com.starikov.datecalc.domain.usecases.GetEventUseCase
import com.starikov.datecalc.domain.usecases.SaveEventDisplaySettingsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EventDetailsViewModel @Inject constructor(
        private val deleteEventUseCase: DeleteEventUseCase,
        private val saveEventDisplaySettingsUseCase: SaveEventDisplaySettingsUseCase,
        getEventUseCase: GetEventUseCase,
        savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val eventId: Long = savedStateHandle.get<Long>("eventId")!!

    val event = getEventUseCase.execute(eventId).asLiveData() as MutableLiveData

    val timestampCounters = Transformations.map(event) { event ->
        event?.displaySettings?.countingMethods?.let {
            event.countForEvent(it)
        } ?: TimestampCounters()
    } as MutableLiveData

    val customColor = MutableLiveData(DefinedColors.CUSTOM.int)

    fun deleteEvent() =
        viewModelScope.launch {
            event.value?.let { deleteEventUseCase.execute(it) }
        }

    fun saveEventDisplaySettings() =
        viewModelScope.launch {
            event.value?.run {
                saveEventDisplaySettingsUseCase.execute(displaySettings)
            }
        }

    fun updateCountingMethod(method: CountingMethod) {
        val safeEventDS = event.value!!.displaySettings
        val mutableSet = safeEventDS.countingMethods.toMutableSet()
        // Метод уже добавлен
        if (mutableSet.contains(method)) {
            mutableSet.remove(method)
        } else {
            mutableSet.add(method)
        }
        val newEventDS = safeEventDS.copy(countingMethods = mutableSet)
        event.value = event.value?.copy(displaySettings = newEventDS)
    }

    fun updateColor(color: DefinedColors) {
        val newColor = if (color != DefinedColors.CUSTOM) {
            color.int
        } else customColor.value!!
        val newEventDS = event.value!!.displaySettings.copy(color = newColor)
        event.value = event.value?.copy(displaySettings = newEventDS)
    }

    fun isCountingMethodSelected(method: CountingMethod) =
        event.value?.displaySettings?.countingMethods?.contains(method) == true ||
            timestampCounters.value?.isSelected(method) == true
}