package com.starikov.datecalc.presentation.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.starikov.datecalc.R
import com.starikov.datecalc.databinding.FragmentDownloadFromFileBinding
import com.starikov.datecalc.domain.common.LoadStatus
import com.starikov.datecalc.presentation.viewmodels.DownloadFromFileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DownloadFromFileFragment : Fragment() {

    private lateinit var binding: FragmentDownloadFromFileBinding
    private val viewModel: DownloadFromFileViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDownloadFromFileBinding.inflate(inflater, container, false).apply {
            viewModel = this@DownloadFromFileFragment.viewModel
            lifecycleOwner = viewLifecycleOwner

            buttonChoose.setOnClickListener {
                if (necessaryPermissionsGranted()) {
                    actionChoose()
                } else {
                    requestNecessaryPermissions()
                }
            }
            fabApply.setOnClickListener { actionApply() }
            subscribeUI()
        }

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val isBackupFileSelected = (
            requestCode == REQUEST_CODE
            && resultCode == Activity.RESULT_OK
            && data?.data != null)
        if (isBackupFileSelected) {
            backupFileSelected(data!!.data!!)
        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.error_backup_file_not_selected),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        val isGranted = (requestCode == PERMISSIONS_CODE
            && grantResults.all { it == PackageManager.PERMISSION_GRANTED } )
        if (isGranted) {
            actionChoose()
        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.load_file_permissions_denied),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun actionChoose() {
        val chooser = Intent.createChooser(
            getGettingFileIntent(), getString(R.string.choose_backup_file_for_download))
        startActivityForResult(chooser, REQUEST_CODE)
    }

    private fun actionApply() {
        viewModel.applyEventsFromBackup()
    }

    private fun backupFileSelected(uri: Uri) {
        viewModel.selectedCorrectBackupFile(uri)
    }

    private fun subscribeUI() {
        viewModel.downloadStatus.observe(viewLifecycleOwner) {
            if (it == LoadStatus.FAIL) {
                Toast.makeText(
                        requireContext(),
                        getString(R.string.error_apply_events_from_backup),
                        Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun getGettingFileIntent() =
        Intent().apply {
            action = Intent.ACTION_GET_CONTENT
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/*"
        }

    private fun necessaryPermissionsGranted() =
        NECESSARY_PERMISSIONS.all {
            ActivityCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
        }

    private fun requestNecessaryPermissions() {
        requestPermissions(NECESSARY_PERMISSIONS, PERMISSIONS_CODE)
    }

    companion object {
        private val NECESSARY_PERMISSIONS = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        private const val PERMISSIONS_CODE = 2

        private const val REQUEST_CODE = 0
    }
}