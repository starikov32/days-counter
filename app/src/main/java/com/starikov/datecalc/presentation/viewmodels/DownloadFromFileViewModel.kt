package com.starikov.datecalc.presentation.viewmodels

import android.net.Uri
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.starikov.datecalc.data.datasources.EventsBackupDataSource
import com.starikov.datecalc.domain.common.InsertionMethod
import com.starikov.datecalc.domain.common.LoadStatus
import com.starikov.datecalc.domain.usecases.ApplyEventsBackupUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DownloadFromFileViewModel @Inject constructor(
        private val applyEventsBackupUseCase: ApplyEventsBackupUseCase
) : ViewModel() {

    val insertionMethod = ObservableField(InsertionMethod.REPLACE)
    val downloadStatus = MutableLiveData(LoadStatus.FILE_NOT_SELECTED)

    val backupFilename = EventsBackupDataSource.BACKUP_FILENAME_FULL
    val backupFileUri: ObservableField<Uri?> = ObservableField(Uri.EMPTY)

    fun applyEventsFromBackup() {
        downloadStatus.postValue(LoadStatus.IN_PROCESS)
        viewModelScope.launch {
            val countOfAddedEvents = applyEventsBackupUseCase.execute(
                backupFileUri.get().toString(),
                insertionMethod.get()
            )
            if (countOfAddedEvents != null && countOfAddedEvents > 0) {
                downloadStatus.postValue(LoadStatus.SUCCESS)
            } else {
                downloadStatus.postValue(LoadStatus.FAIL)
            }
        }
    }

    fun setDownloadStatus(method: InsertionMethod) {
        insertionMethod.set(method)
    }

    fun selectedCorrectBackupFile(uri: Uri) {
        downloadStatus.postValue(LoadStatus.NOT_STARTED)
        backupFileUri.set(uri)
    }
}