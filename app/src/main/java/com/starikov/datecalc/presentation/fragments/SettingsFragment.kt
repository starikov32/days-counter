package com.starikov.datecalc.presentation.fragments

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.*
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.starikov.datecalc.R
import com.starikov.datecalc.data.AppPreferences
import com.starikov.datecalc.domain.common.DateFormatStyle
import com.starikov.datecalc.domain.common.utils.DateFormatter
import com.starikov.datecalc.presentation.viewmodels.SettingsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class SettingsFragment : PreferenceFragmentCompat() {

    private var billingProcessor: BillingProcessor? = null
    private val navController by lazy { findNavController()}
    @Inject
    lateinit var appPreferences: AppPreferences

    private val viewModel: SettingsViewModel by viewModels()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)

        val rateAppPref: Preference = findPreference(RATE_APP_KEY)!!
        val tellFriendsPref: Preference = findPreference(TELL_FRIENDS_KEY)!!
        val contactUsPref: Preference = findPreference(CONTACT_US_KEY)!!
        val makeDonatePref: Preference = findPreference(MAKE_DONATE_KEY)!!
        val uploadToFilePref: Preference = findPreference(UPLOAD_TO_FILE_KEY)!!
        val downloadFromFilePref: Preference = findPreference(DOWNLOAD_FROM_FILE_KEY)!!
        val darkThemePref: SwitchPreference = findPreference(DARK_THEME)!!
        val useSystemThemePref: CheckBoxPreference = findPreference(USE_SYSTEM_THEME)!!
        val dateFormatStylePref: ListPreference = findPreference(DATE_FORMAT_STYLE)!!

        rateAppPref.setOnPreferenceClickListener { actionRateApp(); true }
        tellFriendsPref.setOnPreferenceClickListener { actionTellFriends(); true }
        contactUsPref.setOnPreferenceClickListener { actionContactUs(); true }
        makeDonatePref.setOnPreferenceClickListener { actionMakeDonate(); true }
        uploadToFilePref.setOnPreferenceClickListener { actionUploadToFile(); true }
        downloadFromFilePref.setOnPreferenceClickListener { actionDownloadFromFile(); true }
        darkThemePref.setOnPreferenceClickListener { actionChangeTheme(); true }
        useSystemThemePref.setOnPreferenceClickListener { actionChangeTheme(); true }

        dateFormatStylePref.setOnPreferenceChangeListener { preference, newValue ->
            val dateFormatStyle = DateFormatStyle.toDateFormatStyle(newValue as String)
            appPreferences.dateFormatStyle = dateFormatStyle
            preference.summary = DateFormatter(dateFormatStyle).format(Date(0))
            true
        }
        val dateFormatStyle = DateFormatStyle.toDateFormatStyle(dateFormatStylePref.value as String)
        dateFormatStylePref.summary = DateFormatter(dateFormatStyle).format(Date(0))

        billingProcessor = BillingProcessor(activity, resources.getString(R.string.donate_rsa_key), billingHandler)
        billingProcessor?.initialize()
    }

    // region ACTIONS

    private fun actionRateApp() {
        val packageName: String = requireActivity().applicationContext.packageName
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(GOOGLE_PLAY_PAGE_SHORT + packageName)
        if (isActivityNotStarted(intent)) {
            intent.data = Uri.parse(GOOGLE_PLAY_PAGE_FULL + packageName)
            if (isActivityNotStarted(intent)) {
                Toast.makeText(requireActivity(), R.string.error_opening_google_play, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun actionTellFriends() {
        val text = requireActivity().getText(R.string.tell_friends).toString()
        val title = requireActivity().getText(R.string.choose_app_title).toString()
        val intent = Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, text)
        }
        val chosenIntent = Intent.createChooser(intent, title)
        requireActivity().startActivity(chosenIntent)
    }

    private fun actionContactUs() {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(SUPPORT_EMAIL_ADDRESS))
        val resInfo = requireActivity().packageManager.queryIntentActivities(intent, 0)
        resInfo.find { it?.activityInfo?.packageName == GMAIL_APP_PACKAGE_ID }
        resInfo.let { intent.setPackage(GMAIL_APP_PACKAGE_ID) }
        requireActivity().startActivity(intent)
    }

    private fun actionMakeDonate() {
        AlertDialog.Builder(requireContext()).apply {
            val view = layoutInflater.inflate(R.layout.donate_dialog, null)
            setView(view)

            val donate1 = billingProcessor?.getPurchaseListingDetails(DONATE_PRODUCT_ID)
            val donate2 = billingProcessor?.getPurchaseListingDetails(DONATE_2_PRODUCT_ID)
            val donate3 = billingProcessor?.getPurchaseListingDetails(DONATE_3_PRODUCT_ID)
            val donate4 = billingProcessor?.getPurchaseListingDetails(DONATE_4_PRODUCT_ID)

            val amountDonate = view.findViewById<TextView>(R.id.amount_donate)
            val amountDonate2 = view.findViewById<TextView>(R.id.amount_donate_2)
            val amountDonate3 = view.findViewById<TextView>(R.id.amount_donate_3)
            val amountDonate4 = view.findViewById<TextView>(R.id.amount_donate_4)

            amountDonate.text = donate1?.priceText ?: ""
            amountDonate2.text = donate2?.priceText ?: ""
            amountDonate3.text = donate3?.priceText ?: ""
            amountDonate4.text = donate4?.priceText ?: ""

            val chooseDonate1 = view.findViewById<Button>(R.id.donate_1_choose)
            val chooseDonate2 = view.findViewById<Button>(R.id.donate_2_choose)
            val chooseDonate3 = view.findViewById<Button>(R.id.donate_3_choose)
            val chooseDonate4 = view.findViewById<Button>(R.id.donate_4_choose)

            chooseDonate1.setOnClickListener { billingProcessor?.purchase(requireActivity(), DONATE_PRODUCT_ID) }
            chooseDonate2.setOnClickListener { billingProcessor?.purchase(requireActivity(), DONATE_2_PRODUCT_ID) }
            chooseDonate3.setOnClickListener { billingProcessor?.purchase(requireActivity(), DONATE_3_PRODUCT_ID) }
            chooseDonate4.setOnClickListener { billingProcessor?.purchase(requireActivity(), DONATE_4_PRODUCT_ID) }

            setCancelable(true)
            setTitle(R.string.choose_donate_level)
            setNegativeButton(R.string.close, null)
        }.show()
    }

    private fun actionUploadToFile() {
        viewModel.hasEvents.observe(viewLifecycleOwner) { hasEvents ->
            if (hasEvents) {
                 navController.navigate(R.id.action_upload_to_file)
            } else {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.upload_to_file_error_empty_list),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun actionDownloadFromFile() {
        navController.navigate(R.id.action_download_from_file)
    }

    private fun actionChangeTheme() {
        requireActivity().recreate()
    }

    // endregion

    private fun isActivityNotStarted(intent: Intent): Boolean {
        return try {
            requireActivity().startActivity(intent)
            false
        } catch (e: ActivityNotFoundException) {
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (billingProcessor?.handleActivityResult(requestCode, resultCode, data) != true) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        billingProcessor?.release()
    }

    companion object {
        private const val RATE_APP_KEY = "rate_app"
        private const val TELL_FRIENDS_KEY = "tell_friends"
        private const val CONTACT_US_KEY = "contact_us"
        private const val MAKE_DONATE_KEY = "make_donate"
        private const val UPLOAD_TO_FILE_KEY = "upload_to_file"
        private const val DOWNLOAD_FROM_FILE_KEY = "download_from_file"
        private const val DARK_THEME = "dark_theme"
        private const val USE_SYSTEM_THEME = "use_system_theme"
        private const val DATE_FORMAT_STYLE = "date_format_style"

        private const val DONATE_PRODUCT_ID = "donate"
        private const val DONATE_2_PRODUCT_ID = "donate_2"
        private const val DONATE_3_PRODUCT_ID = "donate_3"
        private const val DONATE_4_PRODUCT_ID = "donate_4"

        private const val GOOGLE_PLAY_PAGE_SHORT = "market://details?id="
        private const val GOOGLE_PLAY_PAGE_FULL = "https://play.google.com/store/apps/details?id="

        private const val GMAIL_APP_PACKAGE_ID = "com.google.android.gm"
        private const val SUPPORT_EMAIL_ADDRESS = "mailto:starikov.mark.apps@gmail.com"
    }

    private val billingHandler = object : BillingProcessor.IBillingHandler {
        override fun onBillingInitialized() {}

        override fun onPurchaseHistoryRestored() {}

        override fun onProductPurchased(productId: String, details: TransactionDetails?) {
            Toast.makeText(requireActivity(), getString(R.string.thanks_for_donate), Toast.LENGTH_SHORT).show()
            // Используем, для того чтобы можно было купить снова
            billingProcessor!!.consumePurchase(productId)
        }

        override fun onBillingError(errorCode: Int, error: Throwable?) {
            Log.d("BillingProcessor", "On Billing Error: $errorCode")
        }
    }
}
