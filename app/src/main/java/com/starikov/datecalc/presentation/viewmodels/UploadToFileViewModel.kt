package com.starikov.datecalc.presentation.viewmodels

import androidx.lifecycle.*
import com.starikov.datecalc.domain.common.LoadStatus
import com.starikov.datecalc.domain.usecases.MakeEventsBackupUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UploadToFileViewModel @Inject constructor(
        private val makeEventsBackupUseCase: MakeEventsBackupUseCase
) : ViewModel() {

    val uploadStatus = MutableLiveData(LoadStatus.NOT_STARTED)
    var backupFilename: String? = null
        private set

    fun uploadEventsToFile() {
        uploadStatus.postValue(LoadStatus.IN_PROCESS)
        viewModelScope.launch {
            backupFilename = makeEventsBackupUseCase.execute()
            val status = backupFilename?.let { LoadStatus.SUCCESS } ?: LoadStatus.FAIL
            uploadStatus.postValue(status)
        }
    }
}