package com.starikov.datecalc.presentation

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.starikov.datecalc.R

import com.starikov.datecalc.data.LocalPreferences
import com.starikov.datecalc.databinding.ActivityMainBinding
import com.starikov.datecalc.domain.common.Theme
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var localPreferences: LocalPreferences

    private val isDonationDialogNeedToShow: Boolean
        get() {
            val preferences = localPreferences
            return (preferences.userHasEvents
                && !preferences.isDonationDialogPresented
                && preferences.entriesCount >= 5)
        }
    private val selectedTheme: Theme
        get() = localPreferences.appTheme

    private val navController by lazy { findNavController(R.id.nav_host_fragment) }
    private val appBarConfiguration by lazy { AppBarConfiguration.Builder(navController.graph).build() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localPreferences.entriesCount++

        delegate.localNightMode = when(selectedTheme) {
            Theme.DEFAULT_THEME -> AppCompatDelegate.MODE_NIGHT_NO
            Theme.SYSTEM_THEME -> AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM
            Theme.DARK_THEME -> AppCompatDelegate.MODE_NIGHT_YES
        }

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setSupportActionBar(binding.toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)

        if (isDonationDialogNeedToShow) {
            donateDialog.show()
            localPreferences.isDonationDialogPresented = true
        }
    }

    private val donateDialog by lazy {
        AlertDialog.Builder(this).apply {
            setTitle(R.string.donate_dialog_title)
            setPositiveButton(R.string.donate_dialog_positive_button) { _: DialogInterface, _: Int ->
                navController.navigate(R.id.settings_dest)
            }
            setNegativeButton(R.string.donate_dialog_negative_button) { _: DialogInterface, _: Int ->
                actionRateApp()
            }
            setMessage(R.string.donate_dialog_message)
        }.create()
    }

    private fun actionRateApp() {
        val packageName: String = applicationContext.packageName
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(GOOGLE_PLAY_PAGE_SHORT + packageName)
        if (isActivityNotStarted(intent)) {
            intent.data = Uri.parse(GOOGLE_PLAY_PAGE_FULL + packageName)
            if (isActivityNotStarted(intent)) {
                Toast.makeText(this, R.string.error_opening_google_play, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun isActivityNotStarted(intent: Intent): Boolean {
        return try {
            startActivity(intent)
            false
        } catch (e: ActivityNotFoundException) {
            true
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController)
    }

    companion object {
        private const val GOOGLE_PLAY_PAGE_SHORT = "market://details?id="
        private const val GOOGLE_PLAY_PAGE_FULL = "https://play.google.com/store/apps/details?id="
    }
}
