package com.starikov.datecalc.presentation.widgets

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.RemoteViews
import androidx.navigation.NavDeepLinkBuilder
import com.starikov.datecalc.R
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.domain.common.utils.*
import com.starikov.datecalc.domain.usecases.GetEventByWidgetIdUseCase
import com.starikov.datecalc.presentation.fragments.EventDetailsFragmentArgs
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.singleOrNull
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import javax.inject.Inject

@AndroidEntryPoint
class EventWidget : AppWidgetProvider() {

    @Inject
    lateinit var getEventByWidgetIdUseCase: GetEventByWidgetIdUseCase

    @OptIn(DelicateCoroutinesApi::class)
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        for (widgetId in appWidgetIds) {
             GlobalScope.launch {
                 val event = getEventByWidgetIdUseCase.execute(widgetId).singleOrNull()
                 updateWidget(context, widgetId, event)
             }
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        super.onDeleted(context, appWidgetIds)
        GlobalScope.launch(IO) {
            for (widgetId in appWidgetIds) {
                val event = getEventByWidgetIdUseCase.execute(widgetId).singleOrNull()
                updateWidget(context, widgetId, event)
            }
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager?,
        widgetId: Int, newOptions: Bundle?
    ) {
        GlobalScope.launch {
            val event = getEventByWidgetIdUseCase.execute(widgetId).singleOrNull()
            updateWidget(context, widgetId, event)
        }
    }

    companion object {
        fun updateWidget(context: Context, widgetId: Int, event: Event?) {
            // Для того чтобы метод не исполнялся, до вызова конфигурационного активити (известный баг)
            if (event == null) {
                return
            }
            val appWidgetManager = AppWidgetManager.getInstance(context)

            val options = appWidgetManager!!.getAppWidgetOptions(widgetId)
            val minWidth = options.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
            val columns: Int = getCellsForSize(minWidth)

            val title: String
            var date = ""
            var eventTimeStatus = ""
            var countedDays = ""
            var textDay = ""

            val widgetView = RemoteViews(context.packageName, R.layout.event_widget_layout)

            val pendingIntent = if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
                title = context.getString(R.string.widget_deleted_event)

                NavDeepLinkBuilder(context)
                    .setGraph(R.navigation.app_navigation)
                    .setDestination(R.id.event_list_dest)
                    .createPendingIntent()
            } else {

                val timestampCounters = event.countForEvent(event.displaySettings.countingMethods)

                title = event.title
                date = SimpleDateFormat.getDateInstance().format(event.date)
                eventTimeStatus = context.resources.getQuantityString(
                    event.getEventTimeStatusResId(), event.countDaysToToday())

                // countedDays
                timestampCounters.let { tc ->
                    tc.years?.let { countedDays += "$it/" }
                    tc.months?.let { countedDays += "$it/" }
                    tc.weeks?.let { countedDays += "$it/" }
                    tc.days?.let { countedDays += "$it/" }
                }
                countedDays = if (countedDays.isEmpty()) {
                    "-"
                } else {
                    countedDays.dropLast(1)
                }

                // textDay
                val res = context.resources
                timestampCounters.let { tc ->
                    tc.years?.let { textDay += "${res.getQuantityString(R.plurals.short_years, it)}/" }
                    tc.months?.let { textDay += "${res.getQuantityString(R.plurals.short_months, it)}/" }
                    tc.weeks?.let { textDay += "${res.getQuantityString(R.plurals.short_weeks, it)}/" }
                    tc.days?.let { textDay += "${res.getQuantityString(R.plurals.short_days, it)}/" }
                }
                textDay = if (textDay.isEmpty()) {
                    "-"
                } else {
                    textDay.dropLast(1)
                }

                NavDeepLinkBuilder(context.applicationContext)
                    .setGraph(R.navigation.app_navigation)
                    .setDestination(R.id.event_details_dest)
                    .setArguments(EventDetailsFragmentArgs(event.id).toBundle())
                    .createPendingIntent()
            }

            if (columns > 1) {
                widgetView.setViewVisibility(R.id.title, View.VISIBLE)
                widgetView.setViewVisibility(R.id.date, View.VISIBLE)
            } else {
                widgetView.setViewVisibility(R.id.title, View.GONE)
                widgetView.setViewVisibility(R.id.date, View.GONE)
            }

            widgetView.setTextViewText(R.id.title, title)
            widgetView.setTextViewText(R.id.date, date)
            widgetView.setTextViewText(R.id.eventTimeStatus, eventTimeStatus)
            widgetView.setTextViewText(R.id.countedDays, countedDays)
            widgetView.setTextColor(R.id.countedDays, event.displaySettings.color)
            widgetView.setTextViewText(R.id.textDay, textDay)
            widgetView.setOnClickPendingIntent(R.id.rootLayout, pendingIntent)

            if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
                appWidgetManager.updateAppWidget(event.widgetId, widgetView)
            } else {
                appWidgetManager.updateAppWidget(widgetId, widgetView)
            }
        }

        private fun getCellsForSize(size: Int): Int {
            var n = 2
            while (70 * n - 30 < size) {
                ++n
            }
            return n - 1
        }
    }
}
