package com.starikov.datecalc.data

import android.content.Context
import androidx.core.content.edit
import com.starikov.datecalc.BuildConfig
import com.starikov.datecalc.domain.common.DateFormatStyle
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPreferences @Inject constructor(@ApplicationContext context: Context) {

    private val preferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE)

    var dateFormatStyle: DateFormatStyle
        get() = DateFormatStyle.toDateFormatStyle(
                preferences.getString(DATE_FORMAT_STYLE, DEFAULT_DATE_FORMAT_STYLE) ?: DEFAULT_DATE_FORMAT_STYLE)
        set(value) = preferences.edit {
            putString(DATE_FORMAT_STYLE, DateFormatStyle.fromDateFormatStyle(value)) }

    companion object {
        private const val DATE_FORMAT_STYLE = "date_format_style"
        private const val DEFAULT_DATE_FORMAT_STYLE = "1"
    }
}