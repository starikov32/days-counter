package com.starikov.datecalc.data.db

import androidx.room.*
import com.starikov.datecalc.data.entities.EventBaseEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface EventBaseDao : BaseDao<EventBaseEntity> {

    @Query("SELECT * FROM events")
    fun getEvents(): List<EventBaseEntity>

    @Query("SELECT COUNT(id) FROM events")
    fun getEventsCount(): Flow<Int>

    @Query("SELECT * FROM events WHERE id = :id")
    fun getEventBase(id: Long): Flow<EventBaseEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertConflictIgnore(events: List<EventBaseEntity>)

    @Query("DELETE FROM events")
    fun deleteAllEvents()

}
