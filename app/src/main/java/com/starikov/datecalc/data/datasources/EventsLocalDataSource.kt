package com.starikov.datecalc.data.datasources

import com.starikov.datecalc.data.db.EventBaseDao
import com.starikov.datecalc.data.db.EventDisplaySettingsDao
import com.starikov.datecalc.data.db.EventWithDisplaySettingsDao
import com.starikov.datecalc.data.entities.EventBaseEntity
import com.starikov.datecalc.data.entities.EventDisplaySettingsEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventsLocalDataSource @Inject constructor(
        private val eventsDao: EventWithDisplaySettingsDao,
        private val eventBaseDao: EventBaseDao,
        private val displaySettingsDao: EventDisplaySettingsDao
) {

    fun getEvents() = eventsDao.getAllEventsWithDisplaySettings()

    fun getEvent(id: Long) = eventsDao.getEventWithDisplaySettings(id)

    fun getEventsCount() = eventBaseDao.getEventsCount()

    fun getEventsList() = eventBaseDao.getEvents()

    fun getEventByWidgetId(widgetId: Int) = eventsDao.getEventWithDisplaySettingsByWidgetId(widgetId)

    fun addEvent(eventBaseEntity: EventBaseEntity, displaySettingsEntity: EventDisplaySettingsEntity) {
        eventBaseDao.insert(eventBaseEntity)
        displaySettingsDao.insert(displaySettingsEntity)
    }

    fun addEventsConflictIgnore(events: List<EventBaseEntity>) = eventBaseDao.insertConflictIgnore(events)

    fun addEventsConflictReplace(events: List<EventBaseEntity>) = eventBaseDao.insert(events)

    fun insertEventDisplaySettings(eventDisplaySettingsEntity: EventDisplaySettingsEntity) =
            displaySettingsDao.insert(eventDisplaySettingsEntity)

    fun editEventsDisplaySettings(eventsDisplaySettings: List<EventDisplaySettingsEntity>) =
            displaySettingsDao.update(eventsDisplaySettings)

    fun editEvent(event: EventBaseEntity) = eventBaseDao.update(event)

    fun deleteEvent(event: EventBaseEntity) {
        eventBaseDao.delete(event)
        displaySettingsDao.deleteEventDisplaySettingsById(event.id)
    }

    fun deleteAllEvents() {
        eventBaseDao.deleteAllEvents()
        displaySettingsDao.deleteAllEventsDisplaySettings()
    }
}
