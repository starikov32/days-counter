package com.starikov.datecalc.data.entities

import android.appwidget.AppWidgetManager
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "events")
data class EventBaseEntity(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = 0,
        @ColumnInfo(name = "title") var title: String,
        @ColumnInfo(name = "description") var description: String,
        @ColumnInfo(name = "date") var date: Date = Calendar.getInstance().time,
        @ColumnInfo(name = "addition_date") val additionOrder: Long = System.currentTimeMillis(),
        @ColumnInfo(name = "widget_id") var widgetId: Int = AppWidgetManager.INVALID_APPWIDGET_ID
)
