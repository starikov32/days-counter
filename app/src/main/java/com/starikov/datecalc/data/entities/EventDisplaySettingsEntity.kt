package com.starikov.datecalc.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.starikov.datecalc.domain.common.CountingMethod
import com.starikov.datecalc.domain.common.DefinedColors

@Entity(tableName = "events_display_settings")
data class EventDisplaySettingsEntity(
    @PrimaryKey @ColumnInfo(name = "event_id")
    val eventId: Long,
    @ColumnInfo(name = "counting_methods")
    val countingMethods: Set<CountingMethod> = CountingMethod.DEFAULT_COUNTING_METHODS,
    @ColumnInfo(name = "order_id")
    var orderId: Long? = null,
    @ColumnInfo(name = "color")
    val color: Int = DefinedColors.DEFAULT_COLOR.int
)