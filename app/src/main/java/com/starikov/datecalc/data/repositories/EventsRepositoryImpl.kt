package com.starikov.datecalc.data.repositories

import com.starikov.datecalc.data.datasources.EventsBackupDataSource
import com.starikov.datecalc.data.datasources.EventsLocalDataSource
import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.domain.common.Event
import com.starikov.datecalc.data.mappers.toEvent
import com.starikov.datecalc.data.mappers.toEventBaseEntity
import com.starikov.datecalc.data.mappers.toEventDisplaySettingsEntity
import com.starikov.datecalc.domain.common.DisplaySettings
import com.starikov.datecalc.domain.common.InsertionMethod
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventsRepositoryImpl @Inject constructor(
        private val localDataSource: EventsLocalDataSource,
        private val backupDataSource: EventsBackupDataSource,
): EventsRepository {

    override fun getEventsList(): Flow<List<Event>> {
        return localDataSource.getEvents().map { events ->
            events.map { it.toEvent() }.sortedBy { it.displaySettings.orderId }
        }
    }

    override fun getEvent(id: Long): Flow<Event> {
        return localDataSource.getEvent(id).map { it.toEvent() }
    }

    override fun hasEvents(): Flow<Boolean> {
        return localDataSource.getEventsCount().map { it > 0 }
    }

    override fun getEventByWidgetId(widgetId: Int): Flow<Event>  {
        return localDataSource.getEventByWidgetId(widgetId).map { it.toEvent() }
    }

    override suspend fun addEvent(event: Event) {
        withContext(IO) {
            localDataSource.addEvent(event.toEventBaseEntity(), event.toEventDisplaySettingsEntity())
        }
    }

    override suspend fun addEventsConflictIgnore(events: List<Event>) {
        withContext(IO) {
            localDataSource.addEventsConflictIgnore(events.map { it.toEventBaseEntity() })
        }
    }

    override suspend fun addEventsConflictReplace(events: List<Event>) {
        withContext(IO) {
            localDataSource.addEventsConflictReplace(events.map { it.toEventBaseEntity() })
        }
    }

    override suspend fun insertEventDisplaySettings(displaySettings: DisplaySettings) {
        withContext(IO) {
            localDataSource.insertEventDisplaySettings(displaySettings.toEventDisplaySettingsEntity())
        }
    }

    override suspend fun editEvent(event: Event) {
        withContext(IO) {
            localDataSource.editEvent(event.toEventBaseEntity())
        }
    }

    override suspend fun editEventsDisplaySettings(displaySettingsList: List<DisplaySettings>) {
        withContext(IO) {
            localDataSource.editEventsDisplaySettings(displaySettingsList.map { it.toEventDisplaySettingsEntity() })
        }
    }

    override suspend fun deleteEvent(event: Event) {
        withContext(IO) {
            localDataSource.deleteEvent(event.toEventBaseEntity())
        }
    }

    override suspend fun deleteAllEvents() {
        withContext(IO) {
            localDataSource.deleteAllEvents()
        }
    }

    override suspend fun makeEventsBackup(): String? = withContext(IO) {
        val localEvents = localDataSource.getEventsList()
        backupDataSource.writeEventsToBackupFile(localEvents)
    }

    override suspend fun applyEventsBackup(uri: String, method: InsertionMethod?): Int? = withContext(IO) {
        val loadedEvents = backupDataSource.readEventsFromBackupFile(uri)?.map { it.toEvent() }
        if (!loadedEvents.isNullOrEmpty()) {
            when(method) {
                InsertionMethod.REPLACE -> addEventsConflictReplace(loadedEvents)
                InsertionMethod.IGNORE -> addEventsConflictIgnore(loadedEvents)
                else -> {
                    deleteAllEvents()
                    addEventsConflictIgnore(loadedEvents)
                }
            }
        }
        return@withContext loadedEvents?.size
    }
}