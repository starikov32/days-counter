package com.starikov.datecalc.data.mappers

import com.starikov.datecalc.data.entities.EventBaseEntity
import com.starikov.datecalc.data.entities.EventDisplaySettingsEntity
import com.starikov.datecalc.data.entities.EventWithDisplaySettingsEntity
import com.starikov.datecalc.domain.common.DisplaySettings
import com.starikov.datecalc.domain.common.Event
import java.util.*

fun Event.toEventBaseEntity() =
    EventBaseEntity(
        id = id,
        title = title,
        description = description,
        date = date,
        additionOrder = additionOrder,
        widgetId = widgetId
    )

fun Event.toEventDisplaySettingsEntity() = 
    EventDisplaySettingsEntity(
        eventId = displaySettings.eventId,
        countingMethods = displaySettings.countingMethods,
        orderId = displaySettings.orderId,
        color = displaySettings.color,
    )

fun EventBaseEntity.toEvent() =
    Event(
        id = id,
        title = title,
        description = description,
        date = date,
        additionOrder = additionOrder,
        widgetId = widgetId,
        displaySettings = DisplaySettings.default(id)
    )

fun EventWithDisplaySettingsEntity.toEvent(): Event {
    return Event(
        id = base.id,
        title = base.title,
        description = base.description,
        date = base.date,
        additionOrder = base.additionOrder,
        widgetId = base.widgetId,
        displaySettings = displaySettings?.toDisplaySettings() ?: DisplaySettings.default(base.id)
    )
}

fun EventDisplaySettingsEntity.toDisplaySettings(): DisplaySettings {
    return DisplaySettings(
        eventId = eventId,
        countingMethods = countingMethods,
        orderId = orderId ?: Date().time,
        color = color,
    )
}

fun DisplaySettings.toEventDisplaySettingsEntity(): EventDisplaySettingsEntity {
    return EventDisplaySettingsEntity(
        eventId = eventId,
        countingMethods = countingMethods,
        orderId = orderId,
        color = color,
    )
}
