package com.starikov.datecalc.data.entities.utils

import com.google.gson.*
import com.starikov.datecalc.data.entities.EventBaseEntity
import java.lang.reflect.Type
import java.util.*

class EventDeserializer : JsonDeserializer<EventBaseEntity> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): EventBaseEntity {
        return json?.asJsonObject?.run {
            EventBaseEntity(
                id = get("id").asLong,
                title = get("title").asString,
                description = get("description").asString,
                date = Date(get("date").asLong),
                additionOrder = get("addition_date").asLong,
                widgetId = get("widget_id").asInt
            )
        } ?: EventBaseEntity(title = "", description = "")
    }
}