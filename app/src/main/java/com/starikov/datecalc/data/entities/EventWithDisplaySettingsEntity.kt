package com.starikov.datecalc.data.entities

import androidx.room.Embedded
import androidx.room.Relation

data class EventWithDisplaySettingsEntity(
    @Embedded
    val base: EventBaseEntity,
    @Relation(parentColumn = "id", entityColumn = "event_id")
    val displaySettings: EventDisplaySettingsEntity?
)