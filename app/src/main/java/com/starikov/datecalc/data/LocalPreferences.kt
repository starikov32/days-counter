package com.starikov.datecalc.data

import android.content.Context
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.starikov.datecalc.domain.common.Theme
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
class LocalPreferences @Inject constructor(@ActivityContext context: Context) {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    var isDonationDialogPresented: Boolean
        get() = preferences.getBoolean(DONATION_DIALOG_PRESENTED, false)
        set(flag) = preferences.edit { putBoolean(DONATION_DIALOG_PRESENTED, flag) }

    var userHasEvents: Boolean
        get() = preferences.getBoolean(USER_HAS_EVENTS, false)
        set(flag) = preferences.edit { putBoolean(USER_HAS_EVENTS, flag) }

    var entriesCount: Int
        get() = preferences.getInt(ENTRIES_COUNT, 0)
        set(value) = preferences.edit { putInt(ENTRIES_COUNT, value) }

    var useRoundIcon: Boolean
        get() = preferences.getBoolean(USE_ROUND_ICON, true)
        set(value) = preferences.edit { putBoolean(USE_ROUND_ICON, value) }

    var appTheme: Theme = Theme.DEFAULT_THEME
        get() {
            val darkTheme = preferences.getBoolean(DARK_THEME, false)
            val useSystemTheme = preferences.getBoolean(USE_SYSTEM_THEME, true)
            return when {
                useSystemTheme -> Theme.SYSTEM_THEME
                darkTheme -> Theme.DARK_THEME
                else -> Theme.DEFAULT_THEME
            }
        }
        private set

    companion object {
        private const val DONATION_DIALOG_PRESENTED = "donation_dialog_presented"
        private const val USER_HAS_EVENTS = "user_has_events"
        private const val ENTRIES_COUNT = "entries_count"

        private const val DARK_THEME = "dark_theme"
        private const val USE_SYSTEM_THEME = "use_system_theme"
        private const val USE_ROUND_ICON = "use_round_button"
    }
}