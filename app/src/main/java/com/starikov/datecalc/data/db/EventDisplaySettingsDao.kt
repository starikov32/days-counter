package com.starikov.datecalc.data.db

import androidx.room.*
import com.starikov.datecalc.data.entities.EventDisplaySettingsEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface EventDisplaySettingsDao : BaseDao<EventDisplaySettingsEntity> {

    @Query("SELECT * FROM events_display_settings")
    fun getAllEventsDisplaySettings(): Flow<List<EventDisplaySettingsEntity>>

    @Query("SELECT * FROM events_display_settings WHERE event_id = :eventId")
    fun getEventDisplaySettings(eventId: Long): Flow<EventDisplaySettingsEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertEventsDisplaySettingsConflictIgnore(eventsDisplaySettings: List<EventDisplaySettingsEntity>)

    @Query("DELETE FROM events_display_settings")
    fun deleteAllEventsDisplaySettings()

    @Query("DELETE FROM events_display_settings WHERE event_id = :eventId")
    fun deleteEventDisplaySettingsById(eventId: Long)
}