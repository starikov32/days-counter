package com.starikov.datecalc.data.db

import android.appwidget.AppWidgetManager
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.starikov.datecalc.data.entities.EventBaseEntity
import com.starikov.datecalc.data.entities.EventDisplaySettingsEntity
import com.starikov.datecalc.domain.common.DefinedColors

@Database(
    entities = [EventBaseEntity::class, EventDisplaySettingsEntity::class],
    version = 5
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun eventBaseDao(): EventBaseDao
    abstract fun eventWithDisplaySettingsDao(): EventWithDisplaySettingsDao
    abstract fun displaySettingsDao(): EventDisplaySettingsDao

    companion object {

        fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, "events_database")
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5)
                    .build()
        }

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE events_table ADD COLUMN widget_id integer DEFAULT ${AppWidgetManager.INVALID_APPWIDGET_ID};")
            }
        }

        private val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE events (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, description TEXT NOT NULL, date INTEGER NOT NULL, addition_date INTEGER NOT NULL, widget_id INTEGER NOT NULL)")
                database.execSQL("INSERT INTO events (id, title, description, date, addition_date, widget_id) SELECT _idinteger, title, description, date, add_id, widget_id FROM events_table")
                database.execSQL("DROP TABLE events_table")
            }
        }

        private val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE events_display_settings (event_id INTEGER PRIMARY KEY NOT NULL, counting_methods INTEGER NOT NULL)")
            }
        }

        private val MIGRATION_4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE events_display_settings ADD COLUMN order_id INTEGER DEFAULT NULL;")
                database.execSQL("ALTER TABLE events_display_settings ADD COLUMN color INTEGER NOT NULL DEFAULT ${DefinedColors.DEFAULT_COLOR};")
            }
        }
    }
}
