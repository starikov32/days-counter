package com.starikov.datecalc.data.datasources

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.starikov.datecalc.data.entities.EventBaseEntity
import com.starikov.datecalc.data.entities.utils.EventDeserializer
import com.starikov.datecalc.data.entities.utils.EventSerializer
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.io.*
import java.lang.reflect.Type
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EventsBackupDataSource @Inject constructor(
    @ApplicationContext appContext: Context
) {

    private val backupPath: String = appContext.filesDir.absolutePath
    private val contentResolver: ContentResolver = appContext.contentResolver

    private val gson by lazy {
        GsonBuilder()
            .registerTypeAdapter(EventBaseEntity::class.java, EventSerializer())
            .registerTypeAdapter(EventBaseEntity::class.java, EventDeserializer())
            .create()
    }

    suspend fun readEventsFromBackupFile(uri: String): List<EventBaseEntity>? = withContext(IO) {
        return@withContext try {
            contentResolver.openFileDescriptor(Uri.parse(uri), "r")?.use { descriptor ->
                FileReader(descriptor.fileDescriptor).use { reader ->
                    val json = reader.readText()
                    val eventListType: Type = object : TypeToken<List<EventBaseEntity>>() {}.type
                    gson.fromJson<List<EventBaseEntity>>(json, eventListType)
                }
            }
        } catch (exception: FileNotFoundException) {
            exception.printStackTrace()
            Log.e(BACKUP_ERROR_TAG, "Error when trying to open backup file. File not found")
            null
        }
    }

    fun writeEventsToBackupFile(events: List<EventBaseEntity>): String? {
        return createNewBackupFile()?.run {
            try {
                FileWriter(this).use {
                    gson.toJson(events, it)
                }
                setReadOnly()
                absolutePath
            } catch (exception: IOException) {
                exception.printStackTrace()
                Log.e(BACKUP_ERROR_TAG, "Write backup in file failed")
                null
            }
        }
    }

    private fun createNewBackupFile(): File? {
        return try {
            File(backupPath, BACKUP_FILENAME_FULL).apply {
                if (exists()) {
                    delete()
                }
                createNewFile()
            }
        } catch (exception: IOException) {
            exception.printStackTrace()
            Log.e(BACKUP_ERROR_TAG, "Create backup file error")
            null
        }
    }

    companion object {
        private const val BACKUP_ERROR_TAG = "BACKUP_ERROR"

        private const val BACKUP_FILENAME = "backup"
        private const val BACKUP_FILENAME_EXT = "txt"
        const val BACKUP_FILENAME_FULL = "$BACKUP_FILENAME.$BACKUP_FILENAME_EXT"
    }
}