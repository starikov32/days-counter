package com.starikov.datecalc.data.entities.utils

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.starikov.datecalc.data.entities.EventBaseEntity
import java.lang.reflect.Type

class EventSerializer : JsonSerializer<EventBaseEntity> {
    override fun serialize(src: EventBaseEntity?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return src?.let { entity ->
            JsonObject().apply {
                addProperty("id", entity.id)
                addProperty("title", entity.title)
                addProperty("description", entity.description)
                addProperty("date", entity.date.time)
                addProperty("addition_date", entity.additionOrder)
                addProperty("widget_id", entity.widgetId)
            }
        } ?: JsonObject()
    }
}