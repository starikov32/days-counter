package com.starikov.datecalc.data.db

import androidx.room.TypeConverter
import com.starikov.datecalc.domain.common.CountingMethod
import java.util.*

class Converters {
    @TypeConverter
    fun dateToTimestamp(date: Date) = date.time
    @TypeConverter
    fun timestampToDate(timestamp: Long) = Date(timestamp)

    @TypeConverter
    fun countingMethodsSetToInt(countingMethods: Set<CountingMethod>): Int {
        // Записать в виде цифры, каждый метод как бит
        var result = 0
        for (method in countingMethods) {
            result = result or method.mask
        }
        return result
    }

    @TypeConverter
    fun intToCountingMethods(mask: Int): Set<CountingMethod> {
        val result = mutableSetOf<CountingMethod>()
        setOf(
            CountingMethod.DAYS,
            CountingMethod.MONTHS,
            CountingMethod.WEEKS,
            CountingMethod.YEARS
        ).forEach {
            if ((mask and it.mask) != 0) {
                result.add(it)
            }
        }
        return result
    }
}