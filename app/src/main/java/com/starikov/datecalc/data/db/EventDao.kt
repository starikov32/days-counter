package com.starikov.datecalc.data.db

import androidx.room.*
import com.starikov.datecalc.data.entities.EventWithDisplaySettingsEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface EventWithDisplaySettingsDao {

    @Transaction
    @Query("SELECT * FROM events")
    fun getAllEventsWithDisplaySettings(): Flow<List<EventWithDisplaySettingsEntity>>

    @Transaction
    @Query("SELECT * FROM events WHERE id = :id")
    fun getEventWithDisplaySettings(id: Long): Flow<EventWithDisplaySettingsEntity>

    @Transaction
    @Query("SELECT * FROM events WHERE widget_id = :widgetId")
    fun getEventWithDisplaySettingsByWidgetId(widgetId: Int): Flow<EventWithDisplaySettingsEntity>

}
