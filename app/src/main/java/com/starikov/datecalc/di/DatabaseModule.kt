package com.starikov.datecalc.di

import android.content.Context
import com.starikov.datecalc.data.db.AppDatabase
import com.starikov.datecalc.domain.repositories.EventsRepository
import com.starikov.datecalc.data.repositories.EventsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.buildDatabase(appContext)

    @Provides
    fun provideEventBaseDao(database: AppDatabase) =
        database.eventBaseDao()

    @Provides
    fun provideEventDao(database: AppDatabase) =
        database.eventWithDisplaySettingsDao()

    @Provides
    fun provideDisplaySettingsDao(database: AppDatabase) =
        database.displaySettingsDao()
}

@Module
@InstallIn(SingletonComponent::class)
abstract class EventRepositoryModule {

    @Singleton
    @Binds
    abstract fun bindEventsRepository(
            analyticsServiceImpl: EventsRepositoryImpl
    ) : EventsRepository
}